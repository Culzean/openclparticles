#ifndef PARTICLE_ARRAY_H
#define PARTICLE_ARRAY_H

#include "stafx.h"
#include "Renderable.h"

class Plane;
class OpenCLHandler;

class ParticleArray : public Renderable{
private:
	int numParticles;
	GLfloat* positions;
	GLfloat* colours;
	GLfloat* vel;
	GLfloat* accel;
	GLfloat* life;

	GLfloat* startPos;
	GLfloat* startVel;

	GLfloat maxDepth;

	OpenCLHandler* clHandler;
	//cl bos
	cl_mem		clVel;
	cl_mem		clPos;
	cl_mem		clAccel;
	cl_mem		clLife;

	cl_mem		clStartPos;
	cl_mem		clStartVel;

	void createParticle(int index);
	void InitArray();
	glm::vec3 norm;
	Plane* spawnPlane;

	GLuint vao;
	GLuint* vbo;
	GLuint iNumBuffers;

public:
	ParticleArray(const int n);
	ParticleArray(const int n, Plane* cSpawnPlane, OpenCLHandler* pHandler);
	virtual ~ParticleArray();
	virtual void Init();
	void bindParticle(void);
	bool createCLBuffers();
	void BindKernel( );
	int getNumParticles(void) const { return numParticles; }
	GLfloat* getPositions(void) const { return positions; }
	GLfloat* getColours(void) const { return colours; }
	GLfloat* getVel(void) const { return vel; }
	virtual void Update();
	virtual void Draw( AGPShader* shader );
	virtual void Draw( AGPShader* shader, glm::vec3 input );
	GLuint	shaderID;
	GLuint	shaderType;
	GLuint texID;
};

#endif
