#include <stdio.h>
#include <string>
#include <sstream>

#include "Renderable.h"
#include "stafx.h"
#include "OpenCLHandler.h"

using namespace std;

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

Renderable*	particle;
Plane*			plane;

AGPShader*			shaderManager;
TextureManager*		texManager;
OpenCLHandler*		clHandler;


GLuint crtShader;

GLfloat rot;
glm::mat4 projection;

AGPMatrixStack*		MVP;
glm::vec3			input(0.0);

////////////////
//always good to have an error method!
void exitFatalError(char *message) {
	std::cout << message << "  " << endl;
	std::cout << SDL_GetError();
	SDL_Quit();
	exit(1);
}

SDL_Window* setupRC(SDL_GLContext &glContext) {

	
	SDL_Window* window;
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
			exitFatalError("Unable to initialize SDL");

	//need to check what version?
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 0);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	window = SDL_CreateWindow("SDL/GLM/OpenGL Demo",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if(!window)
		exitFatalError("Unable to create window");

	glContext = SDL_GL_CreateContext(window);
	if(!window)
		exitFatalError("SDL Window Failed!");

	SDL_GL_SetSwapInterval(1);

	return window;
}

void init(void) {

	//assign render variables
	rot = 0.0;
	projection = glm::perspective(65.0f, 4.0f / 3.0f, 1.0f, 100.0f);
	MVP = new AGPMatrixStack();
	MVP->LoadIdentity();
	shaderManager = new AGPShader();
	clHandler = new OpenCLHandler("res/particleKernel.cl");

	glm::vec3 pos0(-4.0, 0.0, -4.0);
	glm::vec3 pos1(-4.0, 2.0, -9.0);
	glm::vec3 pos2(10.0, 2.0, -9.0);
	glm::vec3 pos3(-4.0, 0.0, -6.0);
	plane = new Plane(pos0,pos1,pos2);
	
	//particle = new ParticleArray(200000, plane);
	particle = new ParticleStandard(200, plane);

	texManager = TextureManager::GetInstance();

	//load values

	particle->setShaderID( shaderManager->initShaderPair("res/particle.vert", "res/particle.frag", AGP_SHADER_PARTICLE) );
	particle->setShaderType( AGP_SHADER_PARTICLE );
	particle->setTexID( texManager->loadTexture( "res/smoke2.bmp" ) );
	particle->Init();
	
	/*particle->bindParticle();
	particle->createCLBuffers(clHandler);
	particle->BindVectorAdd(clHandler);*/

	cout << (char *)glGetString(GL_VENDOR) << endl;
	cout << (char *)glGetString(GL_RENDERER) << endl;

}


bool handleSDLEvent(SDL_Event const &sdlEvent) {
	if(sdlEvent.type == SDL_QUIT)
		return false;
	if(sdlEvent.type == SDL_KEYDOWN)
	{
		//keep your friend close!
		switch( sdlEvent.key.keysym.sym)
		{
			case SDLK_ESCAPE:
				return false;
			case SDLK_w:
				input[1] += 0.1;

				break;
			case SDLK_a:
				input[2] -= 0.1;

				break;
			case SDLK_s:
				input[1] -= 0.1;

				break;
			case SDLK_d:
				input[2] += 0.1;

				break;
			default:
				break;
		}
	}
	return true;
}

void draw(SDL_Window* window) {

	GLuint uniformIndex = 0;
	GLuint texUniform = 0;

	glClearColor(0.0, 0.0, 1.0, 1.0);
	glm::vec3 translateRot(0.1f, 0.0f, 0.1f);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	//bind our index list



	MVP->push();

	MVP->translate(glm::vec3(-7.0f, -4.0f, -6.0f));
	MVP->translate( input );
	
	// draw particles
	shaderManager->useShader( MVP, projection ,particle );
	particle->Draw(shaderManager);
	// Update particles
	particle->Update();


	MVP->pop();


	SDL_GL_SwapWindow(window);
}

void cleanup(void) {
    
	// could also detach shaders
	texManager->cleanUp();
	texManager->DestroyInstance();
	delete MVP;
	delete shaderManager;
	delete particle;
	delete plane;
	delete clHandler;
}


int main(int argc, char* argv[]) {

	SDL_Event	sdlEvent;
	bool		running;

	//SDL_GLContext	glContext;
	SDL_Window*		hWindow;
	SDL_GLContext glContext;

	SDL_Init(SDL_INIT_VIDEO);
	running = true;

	hWindow = setupRC(glContext);

	GLenum err = glewInit();
	// Required on Windows... init GLEW to access OpenGL beyond 1.1
	// remove on other platforms
	if (GLEW_OK != err)
	{	// glewInit failed, something is seriously wrong.
		cout << "glewInit failed, aborting." << endl;
		exit (1);
	}

	init();

	while(running)
	{
		SDL_PollEvent(&sdlEvent);
		running = handleSDLEvent(sdlEvent);

		draw(hWindow);
	}

	cleanup();
	//clear the opengl context
	SDL_GL_DeleteContext(glContext);

	SDL_DestroyWindow(hWindow);
	SDL_Quit();

	return 0;
}

void Update() {

	
}