#include "ParticleArray.h"
#include <cstdlib>
#include <ctime>

ParticleArray::ParticleArray(const int n) : numParticles( n ) {

	if ( numParticles <= 0 ) // trap invalid input
		return;
	InitArray();
}

void ParticleArray::bindParticle() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(iNumBuffers, vbo);

	//and sort the particle system
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind VBO for positions
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
	sizeof(GLfloat), getPositions(), GL_DYNAMIC_DRAW); //DYNAMIC
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);     // Enable attribute index 0

	// Colours data in attribute 1, 3 floats per vertex
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind VBO for colours
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
		sizeof(GLfloat), getColours(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);    // Enable attribute index 1

	glEnable(GL_DEPTH_TEST);

}

ParticleArray::ParticleArray(const int n, Plane* cSpawnPlane, OpenCLHandler* pHandler) : numParticles( n ) {

	if ( numParticles <= 0 ) // trap invalid input
		return;
	this->clHandler = pHandler;
	spawnPlane = cSpawnPlane;
	norm = spawnPlane->getNormal();
	InitArray();
}

void ParticleArray::Init() {
	bindParticle();
	createCLBuffers();
	BindKernel();
}

void ParticleArray::createParticle(int index) {

	GLfloat velScalar = (std::rand() % 10000 - 9000 ) / 100000.0f;

	positions[index] = ( std::rand() % ( static_cast<int>(spawnPlane->getWidth() * 100.0f) ) / 100.0f );
	positions[index+1] = ( std::rand() % ( static_cast<int>(spawnPlane->getDepth() * 100.0f ) ) / 100.0f );		
	positions[index+2] = spawnPlane->findPoint(positions[index], positions[index+1]);

	vel[index] = velScalar * norm.x;
	vel[index+1] = velScalar * norm.y;
	vel[index+2] = velScalar * norm.z;

	//reset values need to be stored ready for binding to the kernel
	this->startPos[index] = positions[index];
	this->startPos[index+1] = positions[index+1];
	this->startPos[index+2] = positions[index+2];

	this->startVel[index] = vel[index];
	this->startVel[index+1] = vel[index+1];
	this->startVel[index+2] = vel[index+2];

	accel[index] = ((std::rand() % 100 - 90 ) / 10000000000.0f) * -norm.x;
	accel[index+1] = (std::rand() % 100 - 90 ) / 100000.0f * -1;
	accel[index+2] = ((std::rand() % 100 - 90 ) / 10000000000.0f) * -norm.z;
	
	life[index] = (std::rand() % 6400 - 3400) / 10.0f;
	life[index+1] = (std::rand() % 6400 - 3400) / 10.0f;
	life[index+2] = (std::rand() % 6400 - 3400) / 10.0f;

}

void ParticleArray::Draw(AGPShader* shader, glm::vec3 input) {

}

void ParticleArray::Draw( AGPShader* shader ) {

	glBindVertexArray(this->vao); // bind member vao

	// particle data updated - so need to resend to GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind VBO 4
	glBufferData(GL_ARRAY_BUFFER, this->getNumParticles() * 3 *
		sizeof(GLfloat), this->getPositions(), GL_DYNAMIC_DRAW);
	// Position data in attribute index 0, 3 floats per vertex
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);     // Enable attribute index 0
	
	glDrawArrays(GL_POINTS, 0, this->getNumParticles() );

	shader->onFinish(this->getShaderType());
}

void ParticleArray::Update( ) {

	glFinish( );
	cl_int status = clEnqueueAcquireGLObjects( clHandler->GetQueue(), 1, &this->clPos , 0, NULL, NULL );
	if(status!= CL_SUCCESS)
		clHandler->PrintCLError( status, "clEnqueueAcquireGLObjects : " );

	//enqueue the Kernel object for execution:
	cl_event wait;
	size_t size = this->numParticles * 3 * sizeof(GLfloat);
	size_t globalWorkSize = this->numParticles * 3;

	status = clEnqueueNDRangeKernel( clHandler->GetQueue(), clHandler->kernel, 1, NULL, &globalWorkSize,
		NULL, 0, NULL, &wait );
	if(status!= CL_SUCCESS)
		clHandler->PrintCLError( status, "clEnqueueNDRangeKernel: " );


	//collect our results!
	clEnqueueReadBuffer(clHandler->GetQueue(), clPos, CL_TRUE, 0, size, this->positions, 
                  0, NULL, &wait);
	clEnqueueReadBuffer(clHandler->GetQueue(), clLife, CL_TRUE, 0, size, this->life, 
                  0, NULL, &wait);


	status = clWaitForEvents( 1, &wait );
	if(status!= CL_SUCCESS)
		clHandler->PrintCLError( status, "clWaitForEvents: " );


	clFinish( clHandler->GetQueue() );
	clEnqueueReleaseGLObjects( clHandler->GetQueue(), 1, &this->clPos, 0, NULL, NULL );

}

void ParticleArray::InitArray() {

	GLuint size = numParticles * 3;

	positions = new GLfloat[size];
	colours = new GLfloat[size];
	vel = new GLfloat[size];
	accel = new GLfloat[size];
	//wasting much space for ease of use runtime
	life = new GLfloat[size];

	//set reset values
	this->startPos = new GLfloat[size];
	this->startVel = new GLfloat[size];
	// lets initialise with some lovely random values!
	std::srand(std::time(0));
	for (int i=0;i<size;i++) {
		
		colours[i] = ( std::rand() % 100 ) / 100.0f;
		
	}
	//intialize each particle
	for (int i=0;i< size ;i+=3) {
		createParticle(i);
		
		//std::cout << "garbage numbers: " << positions[]std::endl;
	}
	iNumBuffers = 2;
	vbo = new GLuint[iNumBuffers];
}

void ParticleArray::BindKernel( ) {

	//buffer object associated with the particle system that uses them
	//so bind the kernel arguments here, which will change depending on particle array type

	cl_int status;
	size_t size = sizeof(GLfloat) * numParticles * 3; 

	status  = clSetKernelArg(clHandler->kernel, 0, sizeof(cl_mem), &this->clAccel);
	status |= clSetKernelArg(clHandler->kernel, 1, sizeof(cl_mem), &this->clVel);
	status |= clSetKernelArg(clHandler->kernel, 2, sizeof(cl_mem), &this->clPos);
	status |= clSetKernelArg(clHandler->kernel, 3, sizeof(cl_mem), &this->clLife);
	status |= clSetKernelArg(clHandler->kernel, 4, sizeof(cl_mem), &this->clStartPos);
	status |= clSetKernelArg(clHandler->kernel, 5, sizeof(cl_mem), &this->clStartVel);
	if(status != CL_SUCCESS) {
		std::cout << "arguement binding failed" << std::endl;
   }
	std::cout << "particle array arguments loaded and ready " << std::endl;
}

bool ParticleArray::createCLBuffers() {

	size_t size = sizeof(GLfloat) * numParticles * 3; 
	cl_int err;

	clPos = clCreateFromGLBuffer( clHandler->GetContext(), CL_MEM_READ_WRITE, vbo[1], &err );
	if(err != CL_SUCCESS || clPos == NULL) {
      std::cout << "error creating buffer object, clPos" << clHandler->clErrorString(err) << std::endl;
	  return false;
	}
	//create context. parameters here set what actions and data flow direction of buffer


	clVel = clCreateBuffer(clHandler->GetContext(), CL_MEM_READ_WRITE, size, NULL , &err);
	if(err != CL_SUCCESS || clVel == NULL) {
      std::cout << "error creating buffer object, clVel" << std::endl;
	  return false;
	}

	err = clEnqueueWriteBuffer( clHandler->GetQueue(), clVel, CL_FALSE, 0 , size, this->vel, 0, NULL, NULL );
	if(err != CL_SUCCESS) {
      std::cout << "error creating buffer object, clPos" << std::endl;
	  return false;
	}

	clAccel = clCreateBuffer(clHandler->GetContext(), CL_MEM_READ_WRITE, size, NULL , &err);
	if(err != CL_SUCCESS || clVel == NULL) {
      std::cout << "error creating buffer object, clAccel" << std::endl;
	  return false;
	}

	err = clEnqueueWriteBuffer( clHandler->GetQueue(), clAccel, CL_FALSE, 0 ,size, this->accel, 0, NULL, NULL );
	if(err != CL_SUCCESS) {
      std::cout << "error creating buffer object, clAccel" << std::endl;
	}

	//cl objects for resetting the particles
	//particle life
	clLife = clCreateBuffer(clHandler->GetContext(), CL_MEM_READ_WRITE, size, NULL , &err);
	if(err != CL_SUCCESS || clVel == NULL) {
      std::cout << "error creating buffer object, clLife" << std::endl;
	  return false;
	}
	
	err = clEnqueueWriteBuffer( clHandler->GetQueue(), clLife, CL_FALSE, 0 ,size, this->life, 0, NULL, NULL );
	if(err != CL_SUCCESS) {
      std::cout << "error creating buffer object, clLife" << std::endl;
	}

	//particle reset values, positions
	this->clStartPos = clCreateBuffer(clHandler->GetContext(), CL_MEM_READ_ONLY, size, NULL , &err);
	if(err != CL_SUCCESS || clVel == NULL) {
      std::cout << "error creating buffer object, clStartPos" << std::endl;
	  return false;
	}

	
	err = clEnqueueWriteBuffer( clHandler->GetQueue(), clStartPos, CL_FALSE, 0 ,size, this->startPos, 0, NULL, NULL );
	if(err != CL_SUCCESS) {
      std::cout << "error creating buffer object, clStartPos" << std::endl;
	}

	//reset values velocity
	this->clStartVel = clCreateBuffer(clHandler->GetContext(), CL_MEM_READ_ONLY, size, NULL , &err);
	if(err != CL_SUCCESS || clVel == NULL) {
      std::cout << "error creating buffer object, clVel" << std::endl;
	  return false;
	}

	err = clEnqueueWriteBuffer( clHandler->GetQueue(), clStartVel, CL_FALSE, 0 ,size, this->startVel, 0, NULL, NULL );
	if(err != CL_SUCCESS) {
      std::cout << "error creating buffer object, clLife" << std::endl;
	}

	std::cout << "buffers created and ready " << std::endl;
	return true;
}

ParticleArray::~ParticleArray() {

	glDeleteBuffers(iNumBuffers, vbo);
	glDeleteVertexArrays(1, &vao);

	clReleaseMemObject(this->clPos);
	clReleaseMemObject(this->clAccel);
	clReleaseMemObject(this->clVel);
	clReleaseMemObject(this->clStartPos);
	clReleaseMemObject(this->clStartVel);
	clReleaseMemObject(this->clLife);

	delete [] positions;
	delete [] colours;
	delete [] vel;
	delete [] accel;
	delete [] life;
	delete [] vbo;
	delete [] startPos;
	delete [] startVel;
}