#include "ParticleStandard.h"
#include <cstdlib>
#include <ctime>

ParticleStandard::ParticleStandard(const int n) : numParticles( n ) {

	if ( numParticles <= 0 ) // trap invalid input
		return;
	InitArray();
}

void ParticleStandard::bindParticle() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(iNumBuffers, vbo);

	//and sort the particle system
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind VBO for positions
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
	sizeof(GLfloat), getPositions(), GL_DYNAMIC_DRAW); //DYNAMIC
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);     // Enable attribute index 0

	// Colours data in attribute 1, 3 floats per vertex
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind VBO for colours
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
		sizeof(GLfloat), getColours(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);    // Enable attribute index 1

	glEnable(GL_DEPTH_TEST);

}

ParticleStandard::ParticleStandard(const int n, Plane* cSpawnPlane) : numParticles( n ) {

	if ( numParticles <= 0 ) // trap invalid input
		return;
	spawnPlane = cSpawnPlane;
	norm = spawnPlane->getNormal();
	InitArray();
}

void ParticleStandard::createParticle(int index) {

	GLfloat velScalar = (std::rand() % 10000 ) / 10000.0f;

	positions[index] = ( std::rand() % ( static_cast<int>(spawnPlane->getWidth() * 100.0f) ) / 100.0f );
	positions[index+1] = ( std::rand() % ( static_cast<int>(spawnPlane->getDepth() * 100.0f ) ) / 100.0f );		
	positions[index+2] = spawnPlane->findPoint(positions[index], positions[index+1]);

	vel[index] = velScalar * norm.x;
	vel[index+1] = velScalar * norm.y;
	vel[index+2] = velScalar * norm.z;

	accel[index] = ( std::rand() % 100 - 90 ) / 100.0f;
	accel[index+1] = ( std::rand() % 100 ) / 100000.0f;
	accel[index+2] = ( std::rand() % 100 - 90 ) / 100.0f;

	life[index] = (std::rand() % 6400 - 3400) / 10.0f;
}

void ParticleStandard::Draw(AGPShader* shader, glm::vec3 input) {

}

void ParticleStandard::Draw( AGPShader* shader ) {

	glBindVertexArray(this->vao); // bind member vao

	// particle data updated - so need to resend to GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind VBO 4
	glBufferData(GL_ARRAY_BUFFER, this->getNumParticles() * 3 *
		sizeof(GLfloat), this->getPositions(), GL_DYNAMIC_DRAW);
	// Position data in attribute index 0, 3 floats per vertex
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);     // Enable attribute index 0
	
	glDrawArrays(GL_POINTS, 0, this->getNumParticles() );

	shader->onFinish(this->getShaderType());
}

void ParticleStandard::Update() {

	for(int i=0, end = numParticles*3; i < end; i+=3) 
	{
		positions[i] += vel[i];
		positions[i+1] += vel[i+1];
		positions[i+2] += vel[i+2];

		//update vel
		vel[i] *= accel[i];
		vel[i+1] += accel[i+1];
		vel[i+2] *= accel[i+2];

		if(--life[i] < 0)
		{
			createParticle(i);
		}
	}
}

void ParticleStandard::Init() {
	this->bindParticle();
}

void ParticleStandard::InitArray() {

	positions = new GLfloat[numParticles * 3];
	colours = new GLfloat[numParticles * 3];
	vel = new GLfloat[numParticles * 3];
	accel = new GLfloat[numParticles * 3];
	//wasting much space for ease of use runtime
	life = new GLfloat[numParticles * 3];

	// lets initialise with some lovely random values!
	std::srand(std::time(0));
	for (int i=0;i<numParticles * 3;i++) {
		
		colours[i] = ( std::rand() % 100 ) / 100.0f;
		
	}
	//set random offset placed within plane
	for (int i=0;i<numParticles * 3;i+=3) {
		createParticle(i);
		
		//std::cout << "garbage numbers: " << positions[]std::endl;
	}
	iNumBuffers = 2;
	vbo = new GLuint[iNumBuffers];
}

ParticleStandard::~ParticleStandard() {

	glDeleteBuffers(iNumBuffers, vbo);
	glDeleteVertexArrays(1, &vao);

	delete [] positions;
	delete [] colours;
	delete [] vel;
	delete [] accel;
	delete [] life;
	delete [] vbo;
}