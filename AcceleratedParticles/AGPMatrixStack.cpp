#include "AGPMatrixStack.h"


AGPMatrixStack::AGPMatrixStack() {
	identity = new glm::mat4(1.0);
	LoadIdentity();
	matrixStack.push(identity);
}

AGPMatrixStack::~AGPMatrixStack() {
	clearStack();
}

void AGPMatrixStack::LoadIdentity() {
	
	int size = matrixStack.size();

	while(size > 1)
	{
		pop();
		size = matrixStack.size();
	}
}

void AGPMatrixStack::rotate(float rad, glm::vec3 dims) {
	*matrixStack.top() = glm::rotate(*matrixStack.top(), rad, dims );
}

void AGPMatrixStack::translate(glm::vec3 dims) {
	*matrixStack.top() = glm::translate(*matrixStack.top(), dims );
}

void AGPMatrixStack::multiMatrix(glm::mat4 mat) {
	*matrixStack.top() *= mat;
}

void AGPMatrixStack::pop() {
	glm::mat4* temp = matrixStack.top();
	matrixStack.pop();
	delete temp;
}

void AGPMatrixStack::push() {
	glm::mat4* temp = new glm::mat4(1.0);
	*temp *= *matrixStack.top();
	matrixStack.push(temp);
}

void AGPMatrixStack::push(glm::mat4 mat) {
	glm::mat4* topMat = new glm::mat4(*matrixStack.top() * mat);
	matrixStack.push(topMat);
}

void AGPMatrixStack::loadMatrix( glm::mat4 mat ) {
	//clears stack down the identity and sets new matrix
	LoadIdentity();
	push(mat);
}

void AGPMatrixStack::setProjection( glm::mat4 proj ) {
	//clears stack down the identity and sets new porjection matrix
	LoadIdentity();
	push(proj);
}

void AGPMatrixStack::clearStack() {
	int size = matrixStack.size();

	while(size > 0)
	{
		pop();
		size = matrixStack.size();
	}
}

glm::mat4 AGPMatrixStack::getMatrix() {
	return *matrixStack.top();
}