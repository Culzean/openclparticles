__kernel
void particle(__global float *pos) {

   int idx = get_global_id(0);

   pos[idx] += 0.5;

}