__kernel
void vecadd(__global float *accel,
            __global float *vel,
            __global float *pos,
			__global float *life,
			__global float *startpos,
			__global float *startvel) {

   int idx = get_global_id(0);

   life[idx] -= 1;

   if(life[idx] < 0)
   {
		life[idx] = 400;
		pos[idx] = startpos[idx];
		vel[idx] = startvel[idx];
   }
   
   pos[idx] += vel[idx];

   vel[idx] += accel[idx];

   if(vel[idx] < 0.005)
   {
	vel[idx] = startvel[idx];
   }
}