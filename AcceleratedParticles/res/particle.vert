// simple vertex shader - simple.vert
#version 130
// OpenGL 3.2 replace above with #version 150

uniform mat4x4 MV;
uniform mat4x4 projection;
uniform vec4 lightPos;
uniform float time;

in vec3 in_Position;
in vec3 in_Color;
in vec3 in_Normal;
in vec2 in_TexCoord;
in vec3 in_Vel;
in vec3 in_Accel;

out vec4 ex_Color;
out vec3 ex_N;
out vec3 ex_V;
out vec3 ex_L;
out vec2 ex_TexCoord;

// simple shader program
// particle vertex program
void main(void) {
	
	vec4 vertexPosition = MV * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;

	ex_Color = vec4(in_Color, 1.0);

}
