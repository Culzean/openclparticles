#include "AGPShader.h"


AGPShader::AGPShader() {
	
}

// loadFile - loads text file into char* fname
// allocates memory - so need to delete after use
char* AGPShader::loadFile(char *fname)
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	std::ifstream file (fname, std::ios::in|std::ios::binary|std::ios::ate);
	if (file.is_open())
	{
		size = (int) file.tellg(); // get file size
		memblock = new char [size+1]; // create buffer w space for null char
		file.seekg (0, std::ios::beg);
		file.read (memblock, size);
		file.close();
		memblock[size] = 0;
		std::cout << "file " << fname << " loaded" << std::endl;
	}
	else
	{
		std::cout << "Unable to open file " << fname << std::endl;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
	}
	return memblock;
}


GLuint AGPShader::initShaderPair(char *vertFile, char *fragFile, AGP_SHADER type)
{
  GLuint p, f, v; // Handles for shader program & vertex and fragment shaders

  v = glCreateShader(GL_VERTEX_SHADER); // Create vertex shader handle
  f = glCreateShader(GL_FRAGMENT_SHADER); // " fragment shader handle

  const char *vertSource = loadFile(vertFile); // load vertex shader source
  const char *fragSource = loadFile(fragFile);  // load frag shader source
	
  // Send the shader source to the GPU
  // Strings here are null terminated - a non-zero final parameter can be
  // used to indicate the length of the shader source instead
  glShaderSource(v, 1, &vertSource,0);
  glShaderSource(f, 1, &fragSource,0);
	
  GLint compiled, linked; // return values for checking for compile & link errors

  // compile the vertex shader and test for errors
  glCompileShader(v);
  glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
  if (!compiled) {
    std::cout << "Vertex shader not compiled." << std::endl;
    printShaderError(v);
  } 

  // compile the fragment shader and test for errors
  glCompileShader(f);
  glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
  if (!compiled) {
    std::cout << "Fragment shader not compiled." << std::endl;
    printShaderError(f);
  } 
	
  p = glCreateProgram(); 	// create the handle for the shader program
  glAttachShader(p,v); // attach vertex shader to program
  glAttachShader(p,f); // attach fragment shader to program


	glBindAttribLocation(p,ATTRIBUTE_VERTEX,"in_Position"); // bind position attribute to location 0
	glBindAttribLocation(p,ATTRIBUTE_COLOR,"in_Colour"); // bind colour attribute to location 1
	glBindAttribLocation(p,ATTRIBUTE_NORMAL,"in_Normal"); //bind normals to attribute location 2
	glBindAttribLocation(p,ATTRIBUTE_TEXTURE0,"in_TexCoord"); //bind texture to attribute 3



  glLinkProgram(p); // link the shader program and test for errors
  glGetProgramiv(p, GL_LINK_STATUS, &linked);
  if(!linked) {
    std::cout << "Program not linked." << std::endl;
    printShaderError(p);
  }

 // glUseProgram(p);  // Make the shader program the current active program

  if(!lookupShader(vertFile, fragFile))
  {
	SHADERDEF shaderEntry;
	strncpy( shaderEntry.fragShaderName, vertFile, MAX_SHADER_NAME_LENGTH);
	strncpy( shaderEntry.fragShaderName, fragFile, MAX_SHADER_NAME_LENGTH);
	shaderEntry.shaderID = p;

	shaderTable.push_back(shaderEntry);
  }
  

  delete [] vertSource; // Don't forget to free allocated memory
  delete [] fragSource; // We allocated this in the loadFile function...

  return p; // Return the shader program handle
}

//@param - vert name, frag name
//check current list of shaders for instances of these names
bool AGPShader::lookupShader(char *vertFile, char *fragFile)
{
	//Linear Search... this isn't supposed to be relied on all the time
	for(unsigned int i = 0; i < shaderTable.size(); i++)
		if((strncmp(vertFile, shaderTable[i].vertexShaderName, MAX_SHADER_NAME_LENGTH) == 0) &&
			(strncmp(fragFile, shaderTable[i].fragShaderName, MAX_SHADER_NAME_LENGTH) == 0))
		{
			std::cout << "Shader program all ready loaded, no need to reload " << fragFile << "  " << vertFile << std::endl;
			return true;
		}

	return false;
}

GLuint AGPShader::useShader( Renderable* pRender ) {
	return useShader( pCrtTransform, crtProj, pRender );
}

//return GLuint ref for shader program
///
GLuint AGPShader::useShader( AGPMatrixStack* MV, glm::mat4 projMat, Renderable* pRender )
{
	this->pCrtTransform = MV;
	this->crtProj = projMat;

	GLuint shader = pRender->getShaderID();
	glUseProgram(shader);
	GLint myLoc;
	int mvpIndex = glGetUniformLocation(shader, "MV");
	int projIndex = glGetUniformLocation(shader, "projection");
	glUniformMatrix4fv(mvpIndex, 1, GL_FALSE, glm::value_ptr( MV->getMatrix() ));
	glUniformMatrix4fv(projIndex, 1, GL_FALSE, glm::value_ptr( projMat ));

	//load material
	myLoc = glGetUniformLocation(shader, "material.ambient");
	glUniform4fv(myLoc, 1, glm::value_ptr(pRender->getAmbient()) );
	myLoc = glGetUniformLocation(shader, "material.diffuse");
	glUniform4fv(myLoc, 1, glm::value_ptr(pRender->getDiffuse()) );
	myLoc = glGetUniformLocation(shader, "material.specular");
	glUniform4fv(myLoc, 1, glm::value_ptr(pRender->getSpecular()) );
	myLoc = glGetUniformLocation(shader, "material.shininess");
	GLfloat tempFloat = pRender->getShininess();
	glUniform1fv(myLoc, 1, &tempFloat);

	switch(pRender->getShaderType()){

		GLuint texUniform;

	case AGP_SHADER_TOON:
		texUniform = glGetUniformLocation(shader, "texture0");
		glUniform1i( texUniform, 0 );

		//bind textures and ready to draw

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_1D, pRender->getTexID());
		break;

	case AGP_SHADER_PHONG:
		texUniform = glGetUniformLocation(shader, "texture0");
		glUniform1i( texUniform, 0 );

		//bind textures and ready to draw

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, pRender->getTexID());
		break;

	case AGP_SHADER_PARTICLE:

		//handle texture
		texUniform = glGetUniformLocation(shader, "texture0");
		glUniform1i( texUniform, 0 );

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, pRender->getTexID());

		// Now draw the particles... as easy as this!
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); //RUGH?
		glDepthMask(0);
		glEnable(GL_BLEND);
		glPointSize(4);

			break;

	case AGP_SHADER_CUBE:

		// set cube map
		texUniform = glGetUniformLocation(shader, "cubeMap");
		glUniform1i( texUniform, 0 );

		//bind textures and ready to draw

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, pRender->getTexID());

			break;

	case AGP_SHADER_DEBUG:

		// set gl state for drawing debug lines
		glDisable(GL_DEPTH_BUFFER);

			break;

	default:
		std::cout << "Shader not recognised! " << shader << std::endl;
		break;

	}

	return 0;
}

//check shader type and reset some states as needed
void AGPShader::onFinish(GLuint const type) {

	switch(type){

		GLuint texUniform;

	case AGP_SHADER_TOON:
		
		break;

	case AGP_SHADER_PHONG:
		
		break;

	case AGP_SHADER_PARTICLE:

		glDisable(GL_BLEND);
		//reset depth mask
		glDepthMask(1);

			break;
	case AGP_SHADER_DEBUG:

		// set gl state for drawing debug lines
		glEnable(GL_DEPTH_BUFFER);

			break;


	default:
		std::cout << "Shader not recognised! " << type << std::endl;
		break;

	}
}

// printShaderError
// Display (hopefully) useful error messages if shader fails to compile or link
void AGPShader::printShaderError(GLint shader)
{
  int maxLength = 0;
  int logLength = 0;
  GLchar *logMessage;

  // Find out how long the error message is
  if (!glIsShader(shader))
    glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
  else
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

  if (maxLength > 0) // If message has length > 0
  {
    logMessage = new GLchar[maxLength];
    if (!glIsShader(shader))
       glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
    else
       glGetShaderInfoLog(shader,maxLength, &logLength, logMessage);
    std::cout << "Shader Info Log:" << std::endl << logMessage << std::endl;
    delete [] logMessage;
  }
}


AGPShader::~AGPShader() {
	glUseProgram(0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);

	for(int i =0; i < shaderTable.size(); ++i)
		glDeleteProgram(shaderTable[i].shaderID);
}