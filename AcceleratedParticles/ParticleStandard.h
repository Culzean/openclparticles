#ifndef PARTICLE_STANDARD_H
#define PARTICLE_STANDARD_H

#include "stafx.h"
#include "Renderable.h"

class Plane;

class ParticleStandard : public Renderable{
private:
	int numParticles;
	GLfloat* positions;
	GLfloat* colours;
	GLfloat* vel;
	GLfloat* accel;
	GLfloat* life;
	void createParticle(int index);
	void InitArray();
	glm::vec3 norm;
	Plane* spawnPlane;

	GLuint vao;
	GLuint* vbo;
	GLuint iNumBuffers;

public:
	ParticleStandard(const int n);
	ParticleStandard(const int n, Plane* cSpawnPlane);
	virtual ~ParticleStandard();
	virtual void Init();
	void bindParticle(void);
	int getNumParticles(void) const { return numParticles; }
	GLfloat* getPositions(void) const { return positions; }
	GLfloat* getColours(void) const { return colours; }
	GLfloat* getVel(void) const { return vel; }

	virtual void Update();
	virtual void Draw( AGPShader* shader );
	virtual void Draw( AGPShader* shader, glm::vec3 input );
	GLuint	shaderID;
	GLuint	shaderType;
	GLuint texID;
};

#endif
