
#include "OpenCLHandler.h"

using namespace std;

OpenCLHandler::OpenCLHandler(const char* fname) {
	bool cleanBuild = CheckPlatform();
	
	if(cleanBuild)
		cleanBuild = CheckPlatform();
	if(cleanBuild)
		cleanBuild = GetDevice();
	if(cleanBuild)
		cleanBuild = BuildContext();
	if(cleanBuild)
		cleanBuild = InitKernel(fname);
}

OpenCLHandler::~OpenCLHandler() {
	//clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);

	delete [] platforms;
	delete [] devices;
}

void OpenCLHandler::PushData() {
	//enqueue kernel
	
}

void OpenCLHandler::ReadBuffer() {
	//pull buffer data off the card and have a look

	
}

char* OpenCLHandler::LoadFile( const char* fname ) {

	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	std::ifstream file (fname, std::ios::in|std::ios::binary|std::ios::ate);
	if (file.is_open())
	{
		size = (int) file.tellg(); // get file size
		memblock = new char [size+1]; // create buffer w space for null char
		file.seekg (0, std::ios::beg);
		file.read (memblock, size);
		file.close();
		memblock[size] = 0;
		std::cout << "file " << fname << " loaded" << std::endl;
	}
	else
	{
		std::cout << "Unable to open file " << fname << std::endl;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
	}
	return memblock;
}


bool OpenCLHandler::CheckPlatform() {

	//query number of platforms
	err = clGetPlatformIDs(0, NULL, &this->iPlatformCount);

	if(err != CL_SUCCESS)
	{
		this->PrintCLError(err, "Error checking for platform ");
	}
	if(iPlatformCount == 0)
	{
		this->PrintCLError(err, "Cannot see any valid platforms! Exiting ");
		return false;
	}
	//procced to load platform
	platforms = new cl_platform_id[iPlatformCount];

	err = clGetPlatformIDs(iPlatformCount, platforms , NULL);

	if(err != CL_SUCCESS)
	{
		PrintCLError(err, "Error loading platforms ");
		return false;
	}
	cout << "platform loaded!" << endl;

	for(GLuint i =0 ; i< this->iPlatformCount; ++i)
	{
		char buf[100];
		err = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR,
                       sizeof(buf), buf, NULL);
		cout << buf << endl;
		err = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME,
                       sizeof(buf), buf, NULL);
		cout << buf << endl << endl << endl << endl;
	}

	return true;
}

bool OpenCLHandler::GetDevice() {

	//find number of devices. so device* is NULL
	err = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, 0, NULL, &this->iDeviceCount);

	if(err != CL_SUCCESS)
	{
		PrintCLError(err, "Error checking for devices ");
	}
	if(iDeviceCount == 0)
	{
		PrintCLError(err, "Cannot see any valid devices! Exiting");
		return false;
	}
	cout << "Number of device found: " << iDeviceCount << endl;

	this->devices = new cl_device_id[iDeviceCount];
	if(devices == NULL) {
		PrintCLError(err, "could not allocate memory for devices");
      return false;
   }
	
	//same call with allocated devices
	err = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, this->iDeviceCount, devices, NULL);
	if(err != CL_SUCCESS)
	{
		PrintCLError(err, "Error loading devices ");
		return false;
	}
	cout << "devices loaded!" << endl;

	//let us know what the devices are, will be handy trying this on different machines
	for(GLuint i =0 ; i< this->iPlatformCount; ++i)
	{
		char buf[100];
		this->workGroup = clGetDeviceInfo(devices[i], CL_DEVICE_ADDRESS_BITS,
                       sizeof(buf), buf, NULL);
		cout << buf << "  Group size : " << workGroup << endl;

		err = clGetDeviceInfo(devices[i], CL_DEVICE_VENDOR,
                       sizeof(buf), buf, NULL);
		cout << buf << endl;
		err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME,
                       sizeof(buf), buf, NULL);
		cout << buf << endl << endl << endl << endl;

		if(err != CL_SUCCESS) {
			cout << "error loading device: " << i << endl;
         return false;
		}
	}
	return true;
}


bool OpenCLHandler::BuildContext() {

	//need to create a combatible context for opengl
	//these are soem setting for windows users to do just that
	cl_context_properties props[ ] =
	{
		CL_GL_CONTEXT_KHR, (cl_context_properties) wglGetCurrentContext( ),
		CL_WGL_HDC_KHR, (cl_context_properties) wglGetCurrentDC( ),
		CL_CONTEXT_PLATFORM, (cl_context_properties) this->platforms[0],
		0
	};

	// Create a context and associate it with the devices
	context = clCreateContext(props, iDeviceCount, devices, NULL, NULL, &err);
	if(err != CL_SUCCESS || context == NULL) {
		PrintCLError(err, "Failed to create context. Exiting");
		return false;
	}

	//create a command queue. This is important for loading instructions to the GPU

	queue = clCreateCommandQueue( context, devices[0], 0, &err );
	if(err != CL_SUCCESS || queue == NULL) {
		PrintCLError(err, "Failed to create queue. Exiting");
		return false;
	}

	return true;
}

bool OpenCLHandler::InitKernel(const char* fname) {
	
	const char* source = LoadFile(fname);

	this->program = clCreateProgramWithSource(context, 1, &source, NULL, &err);

	if(err != CL_SUCCESS) {
		cout << "Could not open this file " << &fname << endl;
		return false;
	}

	iBuildProgram = clBuildProgram(program, iDeviceCount, devices, NULL, NULL, NULL);

	// If there are build errors, print them to the screen
	if(iBuildProgram != CL_SUCCESS) {
		PrintBuildLog(iBuildProgram, program);
		return false;
   }
   else {
      cout << "No build errors!" << endl;
   }

   kernel = clCreateKernel(program, "particle", &err);
   if(err != CL_SUCCESS) {
    cout << "kernel build failed!" << endl;
	return false;
   }
}

void OpenCLHandler::PrintBuildLog(cl_int iBuild, cl_program program) {

	PrintCLError(iBuild, "Program failed to build.");

		cl_build_status buildStatus;
		for(unsigned int i = 0; i < iDeviceCount; i++) {
			
			clGetProgramBuildInfo(program, devices[i], CL_PROGRAM_BUILD_STATUS,
                          sizeof(cl_build_status), &buildStatus, NULL);

         if(buildStatus == CL_SUCCESS) {
            continue;
         }

         char *buildLog;
         size_t buildLogSize;
         clGetProgramBuildInfo(program, devices[i], CL_PROGRAM_BUILD_LOG,
                          0, NULL, &buildLogSize);
         buildLog = (char*)malloc(buildLogSize);
         if(buildLog == NULL) {
            cout << "build error " << i << endl;
         }
         clGetProgramBuildInfo(program, devices[i], CL_PROGRAM_BUILD_LOG,
                          buildLogSize, buildLog, NULL);
         buildLog[buildLogSize-1] = '\0';
         printf("Device %u Build Log:\n%s\n", i, buildLog);   
         free(buildLog);
      }

}

void OpenCLHandler::PrintCLError(cl_int err, std::string message) {
	cout << "Error in cl process " << message << "  " << clErrorString(err) << endl;
}

// Helper function to get error string
// *********************************************************************
const char* OpenCLHandler::clErrorString(cl_int error)
{
    static const char* errorString[] = {
        "CL_SUCCESS",
        "CL_DEVICE_NOT_FOUND",
        "CL_DEVICE_NOT_AVAILABLE",
        "CL_COMPILER_NOT_AVAILABLE",
        "CL_MEM_OBJECT_ALLOCATION_FAILURE",
        "CL_OUT_OF_RESOURCES",
        "CL_OUT_OF_HOST_MEMORY",
        "CL_PROFILING_INFO_NOT_AVAILABLE",
        "CL_MEM_COPY_OVERLAP",
        "CL_IMAGE_FORMAT_MISMATCH",
        "CL_IMAGE_FORMAT_NOT_SUPPORTED",
        "CL_BUILD_PROGRAM_FAILURE",
        "CL_MAP_FAILURE",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "CL_INVALID_VALUE",
        "CL_INVALID_DEVICE_TYPE",
        "CL_INVALID_PLATFORM",
        "CL_INVALID_DEVICE",
        "CL_INVALID_CONTEXT",
        "CL_INVALID_QUEUE_PROPERTIES",
        "CL_INVALID_COMMAND_QUEUE",
        "CL_INVALID_HOST_PTR",
        "CL_INVALID_MEM_OBJECT",
        "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
        "CL_INVALID_IMAGE_SIZE",
        "CL_INVALID_SAMPLER",
        "CL_INVALID_BINARY",
        "CL_INVALID_BUILD_OPTIONS",
        "CL_INVALID_PROGRAM",
        "CL_INVALID_PROGRAM_EXECUTABLE",
        "CL_INVALID_KERNEL_NAME",
        "CL_INVALID_KERNEL_DEFINITION",
        "CL_INVALID_KERNEL",
        "CL_INVALID_ARG_INDEX",
        "CL_INVALID_ARG_VALUE",
        "CL_INVALID_ARG_SIZE",
        "CL_INVALID_KERNEL_ARGS",
        "CL_INVALID_WORK_DIMENSION",
        "CL_INVALID_WORK_GROUP_SIZE",
        "CL_INVALID_WORK_ITEM_SIZE",
        "CL_INVALID_GLOBAL_OFFSET",
        "CL_INVALID_EVENT_WAIT_LIST",
        "CL_INVALID_EVENT",
        "CL_INVALID_OPERATION",
        "CL_INVALID_GL_OBJECT",
        "CL_INVALID_BUFFER_SIZE",
        "CL_INVALID_MIP_LEVEL",
        "CL_INVALID_GLOBAL_WORK_SIZE",
    };

    const int errorCount = sizeof(errorString) / sizeof(errorString[0]);

    const int index = -error;

    return (index >= 0 && index < errorCount) ? errorString[index] : "";

}