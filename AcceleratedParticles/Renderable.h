#pragma once

#ifndef	RENDERABLE_H
#define RENDERABLE_H

#include <string>

class AGPShader;

#include <SDL.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <glew.h>

#include <glm.hpp>
#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>

class Renderable 
{
public:
	Renderable();
	virtual ~Renderable()=0;
	virtual void Init()=0;

	virtual void Draw( AGPShader* shader );
	virtual void Draw( AGPShader* shader, glm::vec3 input );

	virtual void Update()=0;

	GLuint getTexID()			{	return texID;	};
	GLuint getShaderID()		{	return shaderID;	}
	GLuint getShaderType()		{	return shaderType;	}

	void setShaderType(GLuint type)			{	shaderType = type;	}
	void setTexID(GLuint ID )				{	texID = ID;	}
	void setShaderID(GLuint ID)				{	shaderID = ID; }
	void setTexName(std::string fname)		{	texName = fname;	}	

	void LoadMaterials(GLfloat* mat);

	///material properties
	glm::vec4 getAmbient()						{	return ambient;		}
	glm::vec4 getDiffuse()						{	return diffuse;		}
	glm::vec4 getSpecular()						{	return specular;	}
	GLfloat getShininess()						{	return shininess;	}

	void setAmbient( glm::vec4 value )			{	ambient = value;	}
	void setDiffuse( glm::vec4 value )			{	diffuse = value;	}
	void setSpecular( glm::vec4 value )			{	specular = value;	}
	void setShininess( GLfloat value )			{	shininess = value;	}

private:
	GLuint shaderID;
	GLuint shaderType;
	GLuint texID;
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	GLfloat shininess;
	std::string texName;
};

#endif