#ifndef PLANE_H
	#define PLANE_H

#include "stafx.h"

class Plane
{
private:
	glm::vec3 normal;
	glm::vec3 pos0;
	glm::vec3 pos1;
	glm::vec3 pos2;
	glm::vec3 pos3;
	glm::vec3 centre;
	GLfloat width, depth;

	GLfloat		planeDot;
public:
	Plane::Plane(glm::vec3 pos0, glm::vec3 pos1, glm::vec3 pos2);
	~Plane();

	void PrintOut();

	glm::vec3 getNormal()			{	return normal;	}
	glm::vec3 getPos()				{	return centre;	}
	GLfloat		getWidth()			{	return width;	}
	GLfloat		getDepth()			{	return depth;	}

	GLfloat		findPoint( GLfloat x, GLfloat z );

	glm::vec3 FindNormal();
};

#endif