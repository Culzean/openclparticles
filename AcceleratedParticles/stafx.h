#pragma once

#include <SDL.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <glew.h>

#include <glm.hpp>
#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>

#include <CL\opencl.h>

#include <Windows.h>

#include "AGPShader.h"
#include "AGPMatrixStack.h"
#include "OpenCLHandler.h"
#include "ParticleArray.h"
#include "ParticleStandard.h"
#include "Plane.h"
#include "TextureManager.h"

