#include "TextureManager.h"

using namespace std;

int TextureManager::lastID = 0;

TextureManager* TextureManager::pTexManager = 0;


TextureManager::TextureManager()
{
	//first texture to load is null texture.
	//if this cannot be found we have trouble
	//what recourse is there?
	//loadTexture("NullTexture.tga");
}

GLuint TextureManager::getTexID(const char* fname)
{
	index = Texmap.find(fname);

	if(index == Texmap.end())
		{
			if(loadTexture(fname) == -1)//load failed. attempt to load NULL texture
			{
				cout << "This texture cannot be found" << endl;
				index = Texmap.find("NullTexture.tga");
					if(index == Texmap.end())
					{
						cout << "Backup texture cannot be found!! Failed to load texture anything";
						return -1;//total failure. soomething has been put together wrong
					}else
						return Texmap["res\NullTexture.tga"];
			}
		}

		return Texmap[fname];
}


GLuint TextureManager::loadTexture(const char* fname)
{
	GLuint texID = 0;

	//load texture using SDL method
	glGenTextures(1, &texID);

	//loading useing the sdl basic bmp method
	SDL_Surface*	tempSurface;

	tempSurface = SDL_LoadBMP(fname);
	if(!tempSurface)
	{
		cout << "Error Loading bitmap " << fname << endl;
	}

	glActiveTexture(GL_TEXTURE0);

	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

	//Test! for alpha channel
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tempSurface->w, tempSurface->h, 0, GL_RGB, GL_UNSIGNED_BYTE, tempSurface->pixels);

	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tempSurface);

	//fname is the key. Add this texture to the map
	Texmap.insert(pair<string, GLuint>(fname,texID));

	cout << "New texture loaded : " << fname << endl;
	return getTexID(fname);	// return value of texure ID
}

GLuint TextureManager::loadTexture(const char* fname[6]) {

	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
			GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
			GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Y	};

	GLuint texID = 0;

	//gen texture and bind
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

	//loading useing the sdl basic bmp method
	SDL_Surface*	tempSurface;


	for(int i=0; i< 6; ++i)
	{
		tempSurface = SDL_LoadBMP(fname[i]);
		if(!tempSurface)
		{
			cout << "Error Loading bitmap " << fname << endl;
		}

		glTexImage2D(sides[i] ,0 ,
			GL_RGB,tempSurface->w, tempSurface->h, 0,
			GL_RGB, GL_UNSIGNED_BYTE, tempSurface->pixels);

		SDL_FreeSurface(tempSurface);
	}

	
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );


	//glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	
	//fname is the key. Add this texture to the map

	Texmap.insert(pair<string, GLuint>(fname[0],texID));
	

	cout << "New cubemap loaded : " << fname << endl;
	return texID;	// return value of texure ID

}

void TextureManager::cleanUp()
{
	index = Texmap.begin();
	//for(; index != Texmap.end(); index++)
	{
		glDeleteTextures(Texmap.size(), ( &(*index).second ));
	}
}

TextureManager::~TextureManager(){
		

	}