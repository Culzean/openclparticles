//
////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604
#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <map>
#include <string>

#include <iostream>
#include <assert.h>

#include "stafx.h"



class TextureManager {
private:
	static TextureManager*		pTexManager;
	std::map<std::string, GLuint> Texmap;
	std::map<std::string, GLuint>::iterator index;
	static int lastID;
public:
	//method to get singleton instance
	static TextureManager* GetInstance(){
	if(!pTexManager)
		pTexManager = new TextureManager();
	return pTexManager;
	
	}
	static void DestroyInstance(){
		if(pTexManager)
			delete pTexManager;	pTexManager = NULL;

	}
	~TextureManager();
	TextureManager();
	GLuint loadTexture(const char* fname);
	GLuint loadTexture(const char* fname[6]);
	GLuint getTexID(const char* fname);
	void cleanUp();
};

#endif