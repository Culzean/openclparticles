#ifndef AGPSHADER_MANAGER
#define AGPSHADER_MANAGER

#include <vector>

#include "stafx.h"

class AGPMatrixStack;
class Renderable;

#define MAX_SHADER_NAME_LENGTH	64


enum AGP_ATTRIBUTE { ATTRIBUTE_VERTEX = 0, ATTRIBUTE_COLOR, ATTRIBUTE_NORMAL,
					ATTRIBUTE_TEXTURE0, ATTRIBUTE_TEXTURE1, ATTRIBUTE_TEXTURE2, ATTRIBUTE_VELOCITY, ATTRIBUTE_ACCELERATION };

enum AGP_SHADER { AGP_SHADER_SIMPLE =0, AGP_SHADER_PHONG, AGP_SHADER_TOON, AGP_SHADER_PARTICLE, AGP_SHADER_CUBE ,AGP_SHADER_DEBUG };

struct SHADERDEF {
	char vertexShaderName[MAX_SHADER_NAME_LENGTH];
	char fragShaderName[MAX_SHADER_NAME_LENGTH];
	GLuint shaderID;
	AGP_SHADER type;
	};

//should this be a class?
struct light{
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float position[4];
};

class AGPShader {

public:
	AGPShader();
	~AGPShader();

	GLuint initShaderPair(char *vertFile, char *fragFile, AGP_SHADER type);
	bool loadShader(char* fname);
	GLuint useShader( AGPMatrixStack* MV, glm::mat4 projMat , Renderable* pRender  );
	GLuint useShader( Renderable* pRender  );
	void onFinish(GLuint const type);

	bool	lookupShader(char *vertFile, char *fragFile);

protected:

	light* pCrtLight;
	AGPMatrixStack*	pCrtTransform;
	glm::mat4		crtProj;

	char* loadFile(char *fname);
	void printShaderError(GLint shader);
	std::vector<SHADERDEF>		shaderTable;
};

#endif